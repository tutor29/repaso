const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/agendamiento')
    .then( db => console.log(`Database is connected to ${db.connection.name}`))
    .catch( err => console.log(err))