const Cita = require("../models/Cita");


const listarCitas = async (req, res) => {
    try {
        const citas = await Cita.find();
        res.json(citas);
    } catch (error) {
        res.send(error);
    }

}

const guardarCita = async (req, res) => {
    try {
        const nuevaCita = Cita(req.body);
        const resultadoCita = await nuevaCita.save();
        res.json(resultadoCita);
    } catch (error) {
        res.send({message: error._message});
    }
}

const consultarCitas = async (req, res) => {
    const cita = await Cita.find({_id: req.params.id});

}

const eliminarCitas = async (req, res) => {
    const cita = await Cita.findOneAndDelete({_id: req.params.id});

    
}

const actualizarCita = async (req, res) => {
    const cita = await Cita.findOneAndUpdate({_id: req.params.id}, req.body)
}

module.exports = {
    listarCitas,
    guardarCita,
    consultarCitas,
    eliminarCitas,
    actualizarCita
}
