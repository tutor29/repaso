const { urlencoded } = require('express');
const express = require('express');
const router = require('./routes/routes');

// Iniciamos el servidor de express 
const app = express();

// Middlewares 
app.use(express.json());
app.use(urlencoded({
    extended: true,
}))

// Rutas del api
app.use('/api', router);

module.exports = app;