const { Router } = require('express');
const citasController = require('../controllers/citas.controller');

const router = Router();

router.get('/', citasController.listarCitas);
router.post('/', citasController.guardarCita);

router.put('/:id', (req, res) => {
    res.send(req.params.id)
})

router.delete('/:id', (req, res) => {
    res.send(`aquí se eliminan los usuarios con id ${req.params.id}`);
})

module.exports = router;