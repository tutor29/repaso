const routesCitas = require('./citas.routes');
const { Router } = require('express');

const router = Router();

router.use('/citas', routesCitas);


module.exports = router;
