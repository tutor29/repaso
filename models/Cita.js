const mongoose = require('mongoose');

const esquemaCitas = mongoose.Schema({
    fecha: Date,
    hora: String,
    celular: {
        type: Number,
        min: [0, 'Ingrese un numero positivo'],
    },
    descripcion: {
        type: String,
        required: true,
        validate: {
            validator: function() {
                return this.descripcion.length <= 200;
            },
            message: 'la descripcion debe de ser menor a 200 caracteres'
        }
    }
},
{
    versionKey: false,
})

module.exports = mongoose.model('cita', esquemaCitas);